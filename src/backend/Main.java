package backend;

import backend.blackboard.BlackBoardEntry;
import backend.db.DatabaseRepository;
import backend.db.IRepository;
import webservice.soap.SOAPServer;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 * @author asteinbr
 * @date 11/3/11
 */
public class Main {
    private final static Logger LOGGER = Logger.getLogger(Main.class.getName());

    //private static IRepository repositoryMySQL = new DatabaseRepository("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/mydb", "root", "StudyBuddy");
    private static IRepository repositorySQLite = new DatabaseRepository("org.sqlite.JDBC", "jdbc:sqlite:data/db.sqlitedb", "", "");


    private static ArrayList<BlackBoardEntry> blackBoardList = new ArrayList<BlackBoardEntry>();

    private static void createDemoData() {
        blackBoardList.add(new BlackBoardEntry(1, "Suche Villa im Taunus", "Zum 1.3.2012 suche ich eine Villa mit min. 500qm Wohnflaeche im Taunus.", "Murat D.", new Date(), Boolean.TRUE));
        blackBoardList.add(new BlackBoardEntry(2, "Suche Wohnung im Westend", "Hallo ich Suche ein Loft zum 1.1.2012 im Westend.", "Wolfgang P.", new Date(), Boolean.TRUE));
    }

    private static void printData() {
        for (BlackBoardEntry bbe : blackBoardList)
            LOGGER.info(bbe.toString());
    }

    private static void startSOAPServer() {
        try {
            SOAPServer soapServer = new SOAPServer();
            LOGGER.info("SOAPServer is running...");
        } catch (NullPointerException npe) {
            LOGGER.severe(npe.getMessage());
        }
    }

    private static void executeDemoSqlStatements() {
        repositorySQLite.initialize();

        repositorySQLite.addUser("Alexander", "S.");
        repositorySQLite.addUser("Markus", "E.");
        repositorySQLite.addUser("Sinan", "K.");

        repositorySQLite.getUsers();

        repositorySQLite.shutdown();
    }

    public static void main(String[] args) throws Exception {
        createDemoData();
        printData();
        //executeDemoSqlStatements();
        //repositorySQLite.initialize();
        //repositorySQLite.getUsers();
        //repositorySQLite.shutdown();

        //startSOAPServer();
    }
}
