package backend.db;

import java.sql.*;
import java.util.logging.Logger;

/**
 * @author asteinbr , mernst
 * @version 0.3 alpha 201111191008
 * @date 11/15/11
 */
public class DatabaseRepository implements IRepository {
    private final static Logger LOGGER = Logger.getLogger(DatabaseRepository.class.getName());

    private ResultSet rs;
    private Connection con;
    private String driver;
    private String hostname;
    private String user;
    private String password;
    private PreparedStatement pstmt;

    /**
     * @param hostname
     * @param username
     * @param password
     */
    public DatabaseRepository(String driver, String hostname, String username, String password) {
        this.driver = driver;
        this.hostname = hostname;
        this.user = username;
        this.password = password;
    }//endc

    /**
     * Initialize / open connection to database
     */
    @Override
    public void initialize() {
        try {
            Class.forName(driver);
            this.con = DriverManager.getConnection(hostname, user, password);
            LOGGER.info("database connection opened");
        }//end try
        catch (Exception e) {
            //e.printStackTrace();
            LOGGER.severe(e.getMessage());
        }//end catch
    }

    /**
     * Shutdown / close connection to database
     */
    @Override
    public void shutdown() {
        try {
            con.close();
            LOGGER.info("database connection closed");
        }//end try
        catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }//end catch
    }//end connClose()

    /**
     * Add notes to BillBoard
     *
     * @param subject
     * @param message
     * @param userID
     * @param categoryID
     */
    public void addNotes(String subject, String message, int userID, int categoryID) {
        String addNotesStatement = "INSERT INTO "
                + "Notes"
                + "("
                + "Email, "
                + "Phone_Number, "
                + "UserID, "
                + "CategoryID"
                + ") "
                + "VALUES (?,?,?,?)";
        try {
            pstmt = con.prepareStatement(addNotesStatement);
            pstmt.setString(1, subject);
            pstmt.setString(2, message);
            pstmt.setInt(3, userID);
            pstmt.setInt(4, categoryID);
            pstmt.executeUpdate();
            pstmt.close();

            LOGGER.info("Insert ok!");
        }//end try
        catch (SQLException e) {
            LOGGER.severe(e.getMessage());
        }//end catch
    }//end addNotes()

    /**
     * Update notes, e.g. after edit.
     *
     * @param subject
     * @param message
     * @param userID
     * @param categoryID
     * @param noteID
     */
    public void updateNotes(String subject, String message, int userID, int categoryID, int noteID) {
        String updateNotesStatement = "UPDATE "
                + "Notes "
                + "SET "
                + "Subject=?, "
                + "Message=?, "
                + "CategoryID=? "
                + "WHERE "
                + "idNotes=?";
        try {
            pstmt = con.prepareStatement(updateNotesStatement);
            pstmt.setString(1, subject);
            pstmt.setString(2, message);
            pstmt.setInt(3, categoryID);
            pstmt.setInt(4, noteID);
            pstmt.executeUpdate();
            pstmt.close();

            LOGGER.info("Insert ok!");
        }//end try
        catch (SQLException e) {
            LOGGER.severe(e.getMessage());
        }//end catch
    }//end updateNotes()

    /**
     * Set status of note active or reached deadline
     *
     * @param status
     * @param noteID
     */
    public void setStatusOfNotes(boolean status, int noteID) {
        String updateNoteStatusStatement = "UPDATE "
                + "Notes "
                + "SET "
                + "Status=? "
                + "WHERE "
                + "idNotes=?";
        try {
            pstmt = con.prepareStatement(updateNoteStatusStatement);
            pstmt.setBoolean(1, status);
            pstmt.setInt(2, noteID);
            pstmt.executeUpdate();
            pstmt.close();

            LOGGER.info("Insert ok!");
        }//end try
        catch (SQLException e) {
            LOGGER.severe(e.getMessage());
        }//end catch
    }//end setStatusOfNotes()

    /**
     * Method retrieves the number of notes written by an user
     *
     * @param userid numerical identifier of an user. needed to retrieve amount of notes of that user
     * @return <b>number</b> has amount of notes written by user
     */
    public int getNumberNotesOfUser(int userid) {
        int number = 0;
        String countNotes = "SELECT COUNT "
                + "("
                + "Notes"
                + ") "
                + "FROM "
                + "Notes "
                + "WHERE "
                + "UserID = ? ";
        try {
            pstmt = con.prepareStatement(countNotes);
            pstmt.setInt(1, userid);
            //pstmt.addBatch();
            rs = pstmt.executeQuery();

            while (rs.next()) {
                number = rs.getInt(1);
            }
            rs.close();
            pstmt.close();

        }//end try
        catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }//end catch

        return number;

    }//end getNumberNotesOfUser()

    /**
     * Get COUNT of all notes in billboard
     *
     * @return
     */
    public int getCountNotes() {
        int number = 0;
        String countNotes = "SELECT COUNT"
                + "("
                + "Notes"
                + ") "
                + "FROM "
                + "Notes ";
        try {
            pstmt = con.prepareStatement(countNotes);
            //pstmt.setInt();
            //pstmt.addBatch();
            rs = pstmt.executeQuery();

            while (rs.next()) {
                number = rs.getInt(1);
            }
            rs.close();
            pstmt.close();

        }//end try
        catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }//end catch

        return number;

    }//end getCountNotes()

    /**
     * Method to find and retrieve written notes of an user identified by <b>userid</b>
     *
     * @param userid numerical identifier of an user. needed to retrieve notes.
     * @return Array of Notes: Array contains all notes written by user.
     */
    public String[] getNotes(int userid) {
        String[] notes = new String[getCountNotes()];
        String getNotes = "SELECT "
                + "idNotes, "
                + "Subject, "
                + "Message, "
                + "Creation_Date, "
                + "Description "
                + "FROM " + "Notes, "
                + "Category "
                + "WHERE " + "UserID = ? "
                + "AND " + "idCategory=CategoryID ; ";

        int count = 0;
        try {
            pstmt = con.prepareStatement(getNotes);
            pstmt.setInt(1, userid);
            pstmt.addBatch();
            rs = pstmt.executeQuery();

            while (rs.next()) {
                notes[count] = rs.getString(1);
                count++;
            }
            rs.close();
            pstmt.close();
        }//end try
        catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }//end catch

        return notes;

    }//end getNotes()

    /**
     * Add a Category to BillBoard
     *
     * @param description Description of Category
     */
    public void addCategory(String description) {
        String addCategoryStatement = "INSERT INTO "
                + "Category"
                + "("
                + "Description"
                + ") "
                + "VALUES (?)";
        try {
            pstmt = con.prepareStatement(addCategoryStatement);
            pstmt.setString(1, description);
            pstmt.executeUpdate();
            pstmt.close();

            LOGGER.info("Insert ok!");
        }//end try
        catch (SQLException e) {
            LOGGER.severe(e.getMessage());
        }//end catch
    }//end addCategory()


    /**
     * Method to get all notes of all categories
     *
     * @return notes
     */
    public String[] getAllNotes() {
        String[] notes = new String[getCountNotes()];
        String getNotes = "SELECT "
                + "idNotes, "
                + "Subject, "
                + "Message, "
                + "Creation_Date, "
                + "Description, "
                + "Name, "
                + "Last_Name, "
                + "Email, "
                + "Phone_Number "
                + "FROM " + "Notes " + "AS " + "notes, "
                + "Category " + "AS " + "cat, "
                + "User " + "AS " + "user, "
                + "Contact " + "AS " + "cont "
                + "WHERE " + "notes.UserID" + "=" + "idUser "
                + "AND " + "cat.idCategory" + "=" + "CategoryID "
                + "AND " + "user.idUser" + "=" + "cont.UserID "
                + "AND " + "cont.UserID" + "=" + "user.idUser ; ";

        int count = 0;
        try {
            pstmt = con.prepareStatement(getNotes);
            //pstmt.setInt(1, userid);
            //pstmt.addBatch();
            rs = pstmt.executeQuery();

            while (rs.next()) {
                notes[count] = rs.getString(1);
                count++;
            }
            rs.close();
            pstmt.close();
        }//end try
        catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }//end catch
        return notes;
    }//end getAllNotes()

    /**
     * Method to get all notes of a category
     *
     * @return notes
     */
    public String[] getCategoryNotes(int categoryID) {
        String[] notes = new String[getCountNotes()];
        String getNotes = "SELECT idNotes, "
                + "Subject, "
                + "Message, "
                + "Creation_Date, "
                + "Description, Name, "
                + "Last_Name, "
                + "Email, "
                + "Phone_Number"
                + "FROM mydb.Notes AS notes,"
                + "Category AS cat,"
                + " User AS user,"
                + " Contact AS cont "
                + "WHERE notes.UserID = idUser"
                + "AND cat.idCategory=CategoryID"
                + "AND user.idUser=cont.UserID"
                + "AND cont.UserID=user.idUser"
                + "AND idCategory=? ; ";

        int count = 0;
        try {
            pstmt = con.prepareStatement(getNotes);
            pstmt.setInt(1, categoryID);
            //pstmt.addBatch();
            rs = pstmt.executeQuery();

            while (rs.next()) {
                notes[count] = rs.getString(1);
                count++;
            }
            rs.close();
            pstmt.close();
        }//end try
        catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }//end catch

        return notes;

    }//end getAllNotes()

    ///////////////////////////

    /**
     * Returns all users
     *
     * @return
     */
    @Override
    public String[] getUsers() {
        int n = 0;
        String[] usersStr = null;
        String usersStmt = "SELECT * FROM User";
        try {
            pstmt = con.prepareStatement(usersStmt);
            //pstmt.setInt();
            //pstmt.addBatch();
            rs = pstmt.executeQuery();

            while (rs.next()) {
                String firstname = rs.getString("Name");
                String lastname = rs.getString("Last_Name");
                //usersStr[n] = firstname + lastname;
                System.out.println(n + 1 + ": " + "\"" + firstname + " " + lastname + "\",");  //TODO
                n++;
            }
            rs.close();
            pstmt.close();
        }//end try
        catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }//end catch

        return usersStr;
    }

    /**
     * Returns specific user
     *
     * @param userID
     * @return
     */
    @Override
    public String getSpecificUser(int userID) {
        int n = 0;
        String usersSpecificStr = null;
        String usersStmt = "SELECT Name, Last_Name FROM User WHERE idUser=?";
        try {
            pstmt = con.prepareStatement(usersStmt);
            pstmt.setInt(1, userID);
            //pstmt.addBatch();
            rs = pstmt.executeQuery();

            while (rs.next()) {
                String firstname = rs.getString("Name");
                String lastname = rs.getString("Last_Name");
                usersSpecificStr = firstname + lastname;
                System.out.println(": " + firstname + ", " + lastname + "\",");
            }
            rs.close();
            pstmt.close();
        }//end try
        catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }//end catch

        return usersSpecificStr;
    }

    /**
     * Access database to retrieve userID of an existing user. If user not found, exception is thrown.
     *
     * @param username needed to retrieve userID
     * @return <b>userID</b> of an user as a number.
     */
    public int getUserID(String username) {
        int userID = 0;
        String getUser = "SELECT "
                + "b.id, "
                + "b.Name, "
                + "b.Last_Name "
                + "FROM "
                + "User "
                + "b ";
        try {
            pstmt = con.prepareStatement(getUser);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                int theID = rs.getInt("id");
                String firstname = rs.getString("Name");
                String lastname = rs.getString("Last_Name");
                if (username.equalsIgnoreCase(firstname) && username.equalsIgnoreCase(lastname)) {
                    userID = theID;
                }//end if
            }//end while
            rs.close();
            pstmt.close();
        }//end try
        catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }//end catch

        if (userID == 0) {
            throw new SecurityException("Den Namen: " + username
                    + " gibt es nicht in der Datenbank und liefert keinen Wert.");
        }//end if
        else {
            return userID;
        }//end else
    }//end getUserID()

    /**
     * Method to add an user into the data base.
     *
     * @param name     -> given name of user
     * @param lastname -> family name of user
     */
    public void addUser(String name, String lastname) {
        String addUserStatement = "INSERT INTO "
                + "User"
                + "("
                + "Name, "
                + "Last_Name "
                + ") "
                + "VALUES (?,?)";
        try {
            pstmt = con.prepareStatement(addUserStatement);
            pstmt.setString(1, name);
            pstmt.setString(2, lastname);
            pstmt.executeUpdate();
            pstmt.close();

            LOGGER.info("Insert ok!");
        }//end try
        catch (SQLException e) {
            LOGGER.severe(e.getMessage());
        }//end catch
    }//end addUser()

    /**
     * Method to insert contacts of user.
     *
     * @param email
     * @param phonenumber
     * @param userID      numerical identifier of user. foreign key for table CONTACTS, comes from table USER
     */
    public void addContact(String email, String phonenumber, int userID) {
        String addContactStatement = "INSERT INTO "
                + "Contact"
                + "("
                + "Email, "
                + "Phone_Number, "
                + "UserID"
                + ") "
                + "VALUES (?,?,?)";
        try {
            pstmt = con.prepareStatement(addContactStatement);
            pstmt.setString(1, email);
            pstmt.setString(2, phonenumber);
            pstmt.setInt(3, userID);
            pstmt.executeUpdate();
            pstmt.close();

            LOGGER.info("Insert ok!");
        }//end try
        catch (SQLException e) {
            LOGGER.severe(e.getMessage());
        }//end catch
    }//end addContact()

    /**
     * Update contacts after they have changed
     *
     * @param email
     * @param phonenumber
     * @param userID
     */
    public void updateContact(String email, String phonenumber, int userID, int contactID) {
        String updateContactStatement = "UPDATE "
                + "Contact "
                + "SET "
                + "Email=?, "
                + "Phone_Number=?, "
                + "WHERE "
                + "UserID=? "
                + "AND "
                + "idContact=?";
        try {
            pstmt = con.prepareStatement(updateContactStatement);
            pstmt.setString(1, email);
            pstmt.setString(2, phonenumber);
            pstmt.setInt(3, userID);
            pstmt.setInt(4, contactID);
            pstmt.executeUpdate();
            pstmt.close();

            LOGGER.info("Insert ok!");
        }//end try
        catch (SQLException e) {
            LOGGER.severe(e.getMessage());
        }//end catch
    }//end updateContact()
}