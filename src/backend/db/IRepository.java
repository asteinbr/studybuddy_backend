package backend.db;

/**
 * @author asteinbr
 * @date 11/14/11
 */
public interface IRepository {
	/**
	 * Initialize / open connection to database
	 */
	public void initialize();

	/**
	 * Shutdown / close connection to database
	 */
	public void shutdown();

	/**
	 * Add notes to BillBoard
	 *
	 * @param subject
	 * @param message
	 * @param userID
	 * @param categoryID
	 */
	public void addNotes(String subject, String message, int userID, int categoryID);

	/**
	 * Update notes, e.g. after edit.
	 *
	 * @param subject
	 * @param message
	 * @param userID
	 * @param categoryID
	 * @param noteID
	 */
	public void updateNotes(String subject, String message, int userID, int categoryID, int noteID);

	/**
	 * Set status of note active or reached deadline
	 *
	 * @param status
	 * @param noteID
	 */
	public void setStatusOfNotes(boolean status, int noteID);

	/**
	 * Method retrieves the number of notes written by an user
	 *
	 * @param userid numerical identifier of an user. needed to retrieve amount of notes of that user
	 * @return <b>number</b> has amount of notes written by user
	 */
	public int getNumberNotesOfUser(int userid);

	/**
	 * Get COUNT of all notes in billboard
	 *
	 * @return
	 */
	public int getCountNotes();

	/**
	 * Method to find and retrieve written notes of an user identified by <b>userid</b>
	 *
	 * @param userid numerical identifier of an user. needed to retrieve notes.
	 * @return Array of Notes: Array contains all notes written by user.
	 */
	public String[] getNotes(int userid);

	/**
	 * Add a Category to BillBoard
	 *
	 * @param description
	 */
	public void addCategory(String description);

	/**
	 * Method to get all notes of all categories
	 *
	 * @return notes
	 */
	public String[] getAllNotes();

	/**
	 * Method to get all notes of a category
	 *
	 * @return notes
	 */
	public String[] getCategoryNotes(int categoryID);

	///////////////////////////////////////////////////////////////

	/**
	 * Returns all users
	 *
	 * @return
	 */
	public String[] getUsers();

	/**
	 * Returns specific user
	 *
	 * @param userID
	 * @return
	 */
	public String getSpecificUser(int userID);

	/**
	 * Access database to retrieve userID of an existing user. If user not found, exception is thrown.
	 *
	 * @param username needed to retrieve userID
	 * @return <b>userID</b> of an user as a number.
	 */
	public int getUserID(String username);

	/**
	 * Method to add an user into the data base.
	 *
	 * @param name	 given name of user
	 * @param lastname family name of user
	 */
	public void addUser(String name, String lastname);

	/**
	 * Method to insert contacts of user.
	 *
	 * @param email
	 * @param phonenumber
	 * @param userID	  numerical identifier of user. foreign key for table CONTACTS, comes from table USER
	 */
	public void addContact(String email, String phonenumber, int userID);

	/**
	 * Update contacts after they have changed
	 *
	 * @param email
	 * @param phonenumber
	 * @param userID
	 */
	public void updateContact(String email, String phonenumber, int userID, int contactID);
}
