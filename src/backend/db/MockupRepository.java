package backend.db;

import backend.blackboard.BlackBoardEntry;

import java.util.ArrayList;

/**
 * @author asteinbr
 * @date 11/15/11
 */
public class MockupRepository implements IRepository {
	private static ArrayList<BlackBoardEntry> blackBoardListTemp = new ArrayList<BlackBoardEntry>();
	private int id = 0;

	/**
	 * Initialize / open connection to database
	 */
	@Override
	public void initialize()
	{
	}

	/**
	 * Shutdown / close connection to database
	 */
	@Override
	public void shutdown()
	{
	}

	/**
	 * Add notes to BillBoard
	 *
	 * @param subject
	 * @param message
	 * @param userID
	 * @param categoryID
	 */
	@Override
	public void addNotes(String subject, String message, int userID, int categoryID)
	{
	}

	/**
	 * Update notes, e.g. after edit.
	 *
	 * @param subject
	 * @param message
	 * @param userID
	 * @param categoryID
	 * @param noteID
	 */
	@Override
	public void updateNotes(String subject, String message, int userID, int categoryID, int noteID)
	{
	}

	/**
	 * Set status of note active or reached deadline
	 *
	 * @param status
	 * @param noteID
	 */
	@Override
	public void setStatusOfNotes(boolean status, int noteID)
	{
	}

	/**
	 * Method retrieves the number of notes written by an user
	 *
	 * @param userid numerical identifier of an user. needed to retrieve amount of notes of that user
	 * @return <b>number</b> has amount of notes written by user
	 */
	@Override
	public int getNumberNotesOfUser(int userid)
	{
		return 0;
	}

	/**
	 * Get COUNT of all notes in billboard
	 *
	 * @return
	 */
	@Override
	public int getCountNotes()
	{
		return 0;
	}

	/**
	 * Method to find and retrieve written notes of an user identified by <b>userid</b>
	 *
	 * @param userid numerical identifier of an user. needed to retrieve notes.
	 * @return Array of Notes: Array contains all notes written by user.
	 */
	@Override
	public String[] getNotes(int userid)
	{
		return new String[0];
	}

	/**
	 * Add a Category to BillBoard
	 *
	 * @param description
	 */
	@Override
	public void addCategory(String description)
	{
	}

	/**
	 * Method to get all notes of all categories
	 *
	 * @return notes
	 */
	@Override
	public String[] getAllNotes()
	{
		return new String[0];
	}

	/**
	 * Method to get all notes of a category
	 *
	 * @return notes
	 */
	@Override
	public String[] getCategoryNotes(int categoryID)
	{
		return new String[0];
	}

	/**
	 * Returns all users
	 *
	 * @return
	 */
	@Override
	public String[] getUsers()
	{
		return new String[0];
	}

	/**
	 * Returns specific user
	 *
	 * @param userID
	 * @return
	 */
	@Override
	public String getSpecificUser(int userID)
	{
		return null;
	}

	/**
	 * Access database to retrieve userID of an existing user. If user not found, exception is thrown.
	 *
	 * @param username needed to retrieve userID
	 * @return <b>userID</b> of an user as a number.
	 */
	@Override
	public int getUserID(String username)
	{
		return 0;
	}

	/**
	 * Method to add an user into the data base.
	 *
	 * @param name	 given name of user
	 * @param lastname family name of user
	 */
	@Override
	public void addUser(String name, String lastname)
	{
	}

	/**
	 * Method to insert contacts of user.
	 *
	 * @param email
	 * @param phonenumber
	 * @param userID	  numerical identifier of user. foreign key for table CONTACTS, comes from table USER
	 */
	@Override
	public void addContact(String email, String phonenumber, int userID)
	{
	}

	/**
	 * Update contacts after they have changed
	 *
	 * @param email
	 * @param phonenumber
	 * @param userID
	 */
	@Override
	public void updateContact(String email, String phonenumber, int userID, int contactID)
	{
	}
}
