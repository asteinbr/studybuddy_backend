CREATE TABLE user (
    "idUser" INTEGER NOT NULL DEFAULT (0),
    "Name" VARCHAR(20) NOT NULL,
    "Last_Name" VARCHAR(45) NOT NULL
);
CREATE TABLE contact (
    "idContact" INTEGER NOT NULL DEFAULT (0),
    "UserID" INTEGER NOT NULL,
    "Email" VARCHAR(45) NOT NULL,
    "Phone_Number" VARCHAR(20) NOT NULL
);
CREATE TABLE category (
    "idCategory" INTEGER NOT NULL DEFAULT (0),
    "Description" VARCHAR(15) NOT NULL
);
CREATE TABLE notes (
    "idNotes" INTEGER NOT NULL DEFAULT (0),
    "Subject" VARCHAR(45) NOT NULL,
    "Message" VARCHAR(161) NOT NULL,
    "Creation_Date" TIMESTAMP NOT NULL DEFAULT ('CURRENT_TIMESTAMP'),
    "Status" INTEGER NOT NULL DEFAULT ('1'),
    "UserID" INTEGER NOT NULL,
    "CategoryID" INTEGER NOT NULL
);
CREATE INDEX "userid" on user (idUser ASC);
CREATE INDEX "categoryid" on category (idCategory ASC);
CREATE INDEX "contactid" on contact (idContact ASC);
CREATE INDEX "notesid" on notes (idNotes ASC);
