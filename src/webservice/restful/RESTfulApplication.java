package webservice.restful;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * @author: asteinbr
 * @date: 11/21/11
 */

public class RESTfulApplication extends Application
{
	public Set<Class<?>> getClasses()
	{
		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(RESTfulServer.class);

		return classes;
	}
}