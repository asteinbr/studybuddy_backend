package webservice.restful;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author: asteinbr
 * @date: 11/21/11
 */
public class RESTfulClient {
    public static void main(String[] args) throws Exception {
        //URL url = new URL("http://localhost:8080/REST/StudyBuddy/iCal");
        URL url = new URL("http://localhost:8080/StudyBuddy/BlackBoard/iCal");

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            System.err.println(connection.getResponseCode() + " / " + connection.getResponseMessage());
        } else {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
        }

        connection.disconnect();
    }
}