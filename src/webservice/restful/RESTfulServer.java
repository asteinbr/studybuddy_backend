package webservice.restful;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.RuntimeDelegate;
import java.io.File;
import java.net.InetSocketAddress;

/**
 * @author asteinbr
 * @date 11/21/11
 */

@Path("/BlackBoard")
public class RESTfulServer {
    @Path("/Note")
    @GET
    @Produces("text/plain")
    public String getNote(@QueryParam("id") int id) {
        // usage: http://localhost:8080/REST/StudyBuddy/getNote?id=1

        String note = "1;\nSuche Wohnung;\nIch suche eine Wohnung in Frankfurt Sachsenhausen zu sofort. " +
                "Min. 4 Zimmer und max. 1500€;\nDirk Müller;\n2011-11-22 12:32";

        return note;
    }

    @Path("/iCal")
    @GET
    @Produces("text/calendar")
    public Response getICal() {
        File file = new File("data/calendarDummy.ics");
        String mt = new javax.activation.MimetypesFileTypeMap().getContentType(file);

        return Response.ok(file, mt).build();
    }

    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 10);
        HttpContext context = server.createContext("/StudyBuddy");

        HttpHandler handler = RuntimeDelegate.getInstance().createEndpoint(new RESTfulApplication(), HttpHandler.class);
        context.setHandler(handler);

        server.start();
        System.in.read();

        server.stop(0);
    }
}